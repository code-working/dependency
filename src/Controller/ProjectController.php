<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use App\Entity\ProjectDependency;
use App\Repository\ProjectDependencyRepository;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/project/{projectId}", name="app_project_details")
     * @param string $projectId
     * @return Response
     */
    public function project(string $projectId): Response
    {
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->entityManager->getRepository(Project::class);

        // TODO: Put this into parent controller
        $directProjects = $projectRepository->findByState(ProjectState::DIRECT());

        /** @var ProjectDependencyRepository $projectDependencyRepository */
        $projectDependencyRepository = $this->entityManager->getRepository(ProjectDependency::class);

        $project = $projectRepository->find($projectId);

        $dependencies = $projectDependencyRepository->findBy(['projectScope' => $project->getId()]);

        // Load all required projects
        $requiredProjects = [];
        $requirements = [];
        foreach ($dependencies as $dependency) {
            if (isset($requiredProjects[$dependency->getProject()]) === false) {
                $requiredProjects[$dependency->getProject()] = $projectRepository->find($dependency->getProject());
            }
            if (isset($requiredProjects[$dependency->getRequires()]) === false) {
                $requiredProjects[$dependency->getRequires()] = $projectRepository->find($dependency->getRequires());
            }

            $dependencyProject = $requiredProjects[$dependency->getProject()];
            $dependencyRequires = $requiredProjects[$dependency->getRequires()];

            if (isset($requirements[$dependencyRequires->getName()]) === false) {
                $requirements[$dependencyRequires->getName()] = [
                    'project' => $dependencyRequires->getName(),
                    'installed_version' => $dependency->getInstalledVersion(),
                    'required_by' => []
                ];
            }

            $requirements[$dependencyRequires->getName()]['required_by'][] = $dependencyProject->getName() . ' => '
                . $dependency->getRequiredVersion();

        }

        ksort($requirements);

        $dependantProjects = [];

        $dependants = $projectDependencyRepository->findBy(['requires' => $project->getId()]);

        return $this->render(
            'project/project.html.twig',
            compact('directProjects', 'project', 'dependencies', 'requiredProjects', 'requirements', 'dependants')
        );
    }

}