<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use App\Entity\ProjectDependency;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="app_home_index")
     * @return Response
     */
    public function index(): Response
    {
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->entityManager->getRepository(Project::class);

        $directProjects = $projectRepository->findByState(ProjectState::DIRECT());

        $projects = $projectRepository->findAll();

        /** @var ProjectDependency[] $dependencies */
        $dependencies = $this->entityManager->getRepository(ProjectDependency::class)->findAll();

        $highchartsData = [];
        foreach ($dependencies as $dependency) {
            $projectFrom = null;
            $projectTo = null;
            foreach ($projects as $project) {
                if ($project->getId() === $dependency->getProject()) {
                    $projectFrom = $project->getName();
                } elseif ($project->getId() === $dependency->getRequires()) {
                    $projectTo = $project->getName();
                }
            }
            $highchartsData[] = [$projectFrom, $projectTo];
        }

        $cytoscapeData = [
            'nodes' => [],
            'edges' => []
        ];
        foreach ($projects as $project) {
            $node = [
                'data' => [
                    'id' => $project->getId(),
                    'name' => $project->getName(),
                    'weight' => $project->getProjectState()->equals(ProjectState::DIRECT()) ? 50 : 20
                ]
            ];
            $cytoscapeData['nodes'][] = $node;
        }
        foreach ($dependencies as $dependency) {
            $cytoscapeData['edges'][] = [
                'data' => [
                    'id' => $dependency->getId(),
                    'source' => $dependency->getProject(),
                    'target' => $dependency->getRequires()
                ]
            ];
        }

        return $this->render(
            'home/index.html.twig',
            [
                'directProjects' => $directProjects,
                'projects' => $projects,
                'dependencies' => $dependencies,
                'highchartsData' => $highchartsData,
                'cytoscapeData' => $cytoscapeData
            ]
        );
    }

}