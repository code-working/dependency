<?php

namespace App\Repository;

use App\Entity\ProjectDependency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectDependency|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectDependency|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectDependency[]    findAll()
 * @method ProjectDependency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectDependencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectDependency::class);
    }

    public function removeByProjectScope(string $projectScopeId): void
    {
        $queryBuilder = $this->createQueryBuilder('d');
        $queryBuilder->delete()
            ->where(
                $queryBuilder->expr()->eq('d.projectScope', ':projectScope')
            )
            ->setParameter('projectScope', $projectScopeId);
        $query = $queryBuilder->getQuery();
        $query->execute();
    }

    public function isRequired(string $projectId): bool
    {
        $queryBuilder = $this->createQueryBuilder('d');
        $count = (int)$queryBuilder->select($queryBuilder->expr()->count('d.id'))
            ->andWhere('d.requires = :projectId')
            ->setParameter('projectId', $projectId)
            ->getQuery()
            ->getSingleScalarResult();

        return $count !== 0;
    }

    // /**
    //  * @return ProjectDependency[] Returns an array of ProjectDependency objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectDependency
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
