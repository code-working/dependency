<?php

namespace App\Repository;

use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    /**
     * @param ProjectState $projectState
     *
     * @return Project[]
     */
    public function findByState(ProjectState $projectState): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.projectState = :projectState')
            ->addOrderBy('p.name', 'ASC')
            ->setParameter('projectState', $projectState)
            ->getQuery()
            ->getResult();
    }

    public function findByName(string $name): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
