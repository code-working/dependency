<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\ComposerJson;
use App\Entity\ComposerLock;
use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use App\Entity\ProjectDependency;
use App\Repository\ProjectRepository;
use App\Service\ProjectDownloadService;
use App\Service\ProjectResolveService;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class ProjectAddCommand
 *
 * @author Joachim Beig
 */
class ProjectAddCommand extends Command
{
    private const COMMAND_NAME = 'project:add';

    private ProjectDownloadService $repositoryDownloadService;
    private ProjectResolveService $projectResolveService;
    private EntityManagerInterface $entityManager;

    private ProjectRepository $projectRepository;

    public function __construct(
        ProjectDownloadService $repositoryDownloadService,
        ProjectResolveService $projectResolveService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct();

        $this->repositoryDownloadService = $repositoryDownloadService;
        $this->projectResolveService = $projectResolveService;
        $this->entityManager = $entityManager;

        $this->projectRepository = $this->entityManager->getRepository(Project::class);
    }

    protected function configure()
    {
        $this->setName(static::COMMAND_NAME);

        $this->setDescription('Adds a project');

        $this->addArgument('source', InputArgument::REQUIRED, 'Project Source');

        $this->addArgument('name', InputArgument::OPTIONAL, 'Project Name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectSource = $input->getArgument('source');
        $projectName = $input->getArgument('name');

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        // Download project
        $output->writeln('Clone project...');
        $project = $this->repositoryDownloadService->downloadProject(
            ProjectState::DIRECT(),
            $projectSource,
            $projectName
        );

        $existingProject = $this->projectRepository->findByName($project->getName());

        if ($existingProject !== null) {
            $questionText = sprintf(
                'Project %s already exists with source %s. Do you want to update it? (y/N) ',
                $existingProject->getName(),
                $existingProject->getSource()
            );
            $question = new ConfirmationQuestion($questionText, false);
            $answer = $helper->ask($input, $output, $question);

            if ($answer === false) {
                $output->writeln('Exiting...');
                return 0;
            }

            // Remove dependencies
            $queryBuilder = $this->entityManager->createQueryBuilder();
            $queryBuilder
                ->delete('App:ProjectDependency', 'd')
                ->where(
                    $queryBuilder->expr()->eq('d.projectScope', ':projectScope')
                )
                ->setParameter('projectScope', $existingProject->getId());

            $query = $queryBuilder->getQuery();
            $output->writeln($query->getDQL());
            $query->execute();

            // Update the existing project
            $existingProject->setProjectState(ProjectState::DIRECT());
            $existingProject->setSource($project->getSource());
            $existingProject->setComposerJson($project->getComposerJson());
            $existingProject->setComposerLock($project->getComposerLock());

            $project = $existingProject;
        }

        // Load most recent data from packagist
        $output->writeln('Load data from packagist...');
        $this->projectResolveService->addPackagistDataToProject($project);

        $output->writeln(sprintf('Persist project %s', $project->getName()));
        $this->entityManager->persist($project);

        $output->writeln('Import dependencies');
        $this->importDependencies($project);

        $this->entityManager->flush();

        $this->repositoryDownloadService->clearDownloadCache();

        $output->writeln('Import completed');

        return 0;
    }

    private function importDependencies(Project $project): void
    {
        $projectDownloadDir = $this->repositoryDownloadService->getProjectDownloadDir($project);
        $vendor = $projectDownloadDir . DIRECTORY_SEPARATOR . 'vendor';

        // Get all installed packages from project scope
        $packages = $project->getComposerLock()->getAllPackages();
        $requiredProjects = $project->getComposerJson()->getAllRequired();

        $installedProjects = [];

        foreach ($packages as $package) {
            $packageDir = $vendor . DIRECTORY_SEPARATOR . $package->getName();

            $composerJsonFile = $packageDir . DIRECTORY_SEPARATOR . ComposerJson::FILE_NAME;
            $composerLockFile = $packageDir . DIRECTORY_SEPARATOR . ComposerLock::FILE_NAME;

            if (is_file($composerJsonFile) === false) {
                throw new RuntimeException(
                    sprintf('No %s found for package %s', ComposerJson::FILE_NAME, $package->getName())
                );
            }

            $composerJson = ComposerJson::fromFile($composerJsonFile);
            $composerLock = ComposerLock::fromFile($composerLockFile);

            $installedProject = $this->projectRepository->findByName($package->getName());

            if ($installedProject === null) {
                $installedProject = new Project(
                    $package->getName(),
                    $package->getSource(),
                    ProjectState::DEPENDENCY()
                );
            } else {
                $installedProject->setSource($package->getSource());
            }

            $installedProject->setComposerJson($composerJson);
            $installedProject->setComposerLock($composerLock);

            $this->projectResolveService->addPackagistDataToProject($installedProject);

            $this->entityManager->persist($installedProject);

            $installedProjects[$installedProject->getName()] = $installedProject;

            // Create first-level dependency
            if (isset($requiredProjects[$package->getName()]) === true) {
                $dependency = new ProjectDependency(
                    $project->getId(),
                    $project->getId(),
                    $installedProject->getId(),
                    $package->isDev(),
                    $requiredProjects[$package->getName()],
                    $package->getVersion()
                );
                $this->entityManager->persist($dependency);
            }
        }

        // Create second-level dependencies
        foreach ($installedProjects as $installedProject) {
            $this->importPackageDependencies($project, $installedProject, $installedProjects);
        }
    }

    private function importPackageDependencies(Project $projectScope, Project $project, array $installedProjects): void
    {
        $installedPackages = $projectScope->getComposerLock()->getAllPackages();
        $requiredProjects = $project->getComposerJson()->getRequire();

        foreach ($requiredProjects as $requiredName => $requiredVersion) {
            if (isset($installedPackages[$requiredName]) === false) {
                continue;
            }

            $package = $installedPackages[$requiredName];

            $dependentProject = $installedProjects[$requiredName];

            $dependency = new ProjectDependency(
                $projectScope->getId(),
                $project->getId(),
                $dependentProject->getId(),
                false,
                $requiredVersion,
                $package->getVersion()
            );
            $this->entityManager->persist($dependency);
        }
    }

}