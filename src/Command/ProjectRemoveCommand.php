<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use App\Entity\ProjectDependency;
use App\Repository\ProjectDependencyRepository;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProjectRemoveCommand
 *
 * @author Joachim Beig
 */
class ProjectRemoveCommand extends Command
{
    private const COMMAND_NAME = 'project:remove';

    private EntityManagerInterface $entityManager;

    private ProjectRepository $projectRepository;
    private ProjectDependencyRepository $projectDependencyRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;

        $this->projectRepository = $this->entityManager->getRepository(Project::class);
        $this->projectDependencyRepository = $this->entityManager->getRepository(ProjectDependency::class);
    }

    protected function configure()
    {
        $this->setName(static::COMMAND_NAME);

        $this->setDescription('Removes a project');

        $this->addArgument('name', InputArgument::REQUIRED, 'Project Name');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectName = $input->getArgument('name');

        $project = $this->projectRepository->findByName($projectName);

        if ($project === null) {
            $output->writeln(sprintf('<error>Project "%s" was not found</error>', $projectName));
            return 1;
        }

        if ($project->getProjectState()->equals(ProjectState::DIRECT()) === false) {
            $output->writeln(
                sprintf('<error>Project "%s" was not directly added and cannot be removed.</error>', $projectName)
            );
        }

        $this->projectDependencyRepository->removeByProjectScope($project->getId());

        $projectIsRequired = $this->projectDependencyRepository->isRequired($project->getId());

        if ($projectIsRequired === true) {
            $project->setProjectState(ProjectState::DEPENDENCY());
            $this->entityManager->persist($project);
        } else {
            $this->entityManager->remove($project);
        }

        $this->entityManager->flush();

        $output->writeln(sprintf('Project "%s" removed', $project->getName()));

        return 0;
    }
}