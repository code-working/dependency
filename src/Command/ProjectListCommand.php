<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProjectListCommand
 *
 * @author Joachim Beig
 */
class ProjectListCommand extends Command
{
    private const COMMAND_NAME = 'project:list';

    private EntityManagerInterface $entityManager;

    private ProjectRepository $projectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;

        $this->projectRepository = $this->entityManager->getRepository(Project::class);
    }

    protected function configure(): void
    {
        $this->setName(static::COMMAND_NAME);

        $this->setDescription('Lists all projects');

        $this->addOption('all', null, InputOption::VALUE_NONE, 'Show (all) projects and libraries');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $all = $input->getOption('all');

        $projectCriteria = [];

        if ($all === false) {
            $projectCriteria['projectState'] = ProjectState::DIRECT()->getValue();
        }

        /** @var Project[] $projects */
        $projects = $this->projectRepository->findBy(
            $projectCriteria,
            ['name' => 'ASC']
        );

        // Get the maximum lengths
        $maxName = 0;
        $maxSource = 0;
        $maxState = 0;

        foreach ($projects as $project) {
            $nameLength = mb_strlen($project->getName());
            $sourceLength = mb_strlen($project->getSource());
            $stateLength = mb_strlen($project->getProjectState()->getValue());

            if ($nameLength > $maxName) {
                $maxName = $nameLength;
            }
            if ($sourceLength > $maxSource) {
                $maxSource = $sourceLength;
            }
            if ($stateLength > $maxState) {
                $maxState = $stateLength;
            }
        }

        $outputPattern = '| %-' . $maxName . 's | %-' . $maxSource . 's | %-' . $maxState . 's |';
        $divider = '+' . str_repeat('-', $maxName + 2)
            . '+' . str_repeat('-', $maxSource + 2)
            . '+' . str_repeat('-', $maxState + 2)
            . '+';

        $output->writeln($divider);
        $output->writeln(sprintf($outputPattern, 'name', 'source', 'state'));
        $output->writeln($divider);

        foreach ($projects as $project) {
            $output->writeln(
                sprintf(
                    $outputPattern,
                    $project->getName(),
                    $project->getSource(),
                    $project->getProjectState()->getValue()
                )
            );
        }

        $output->writeln($divider);

        return 0;
    }
}