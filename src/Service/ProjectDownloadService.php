<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ComposerJson;
use App\Entity\ComposerLock;
use App\Entity\Enum\ProjectState;
use App\Entity\Project;
use Ramsey\Uuid\Uuid;
use RuntimeException;

/**
 * Class ProjectDownloadService
 *
 * @author Joachim Beig
 */
class ProjectDownloadService
{
    private string $gitTmpPath;

    public function __construct(string $gitTmpPath)
    {
        $this->gitTmpPath = $gitTmpPath;

        if (!is_dir($this->gitTmpPath)) {
            mkdir($this->gitTmpPath, 0744, true);
        }
    }

    public function downloadProject(
        ProjectState $projectState,
        string $projectSource,
        string $projectName = null,
        string $version = null
    ): Project {
        $tmpDownloadDir = $this->getProjectDownloadDir();

        // Clone project
        $command = 'git clone --depth 1 --quiet';
        if ($version !== null) {
            $command .= ' --branch "' . $version . '"';
        }
        $command .= ' "' . $projectSource . '"' . ' ' . $tmpDownloadDir;
        system($command);

        // Install project dependencies via composer
        if ($projectState->equals(ProjectState::DIRECT())) {
            $command = 'composer --working-dir="' . $tmpDownloadDir . '" install --quiet';
            system($command);
        }

        $composerJsonFile = $tmpDownloadDir . DIRECTORY_SEPARATOR . ComposerJson::FILE_NAME;
        $composerLockFile = $tmpDownloadDir . DIRECTORY_SEPARATOR . ComposerLock::FILE_NAME;

        if (is_file($composerJsonFile) === false) {
            throw new RuntimeException(sprintf('No %s found', ComposerJson::FILE_NAME));
        }

        // Read composer.json
        $composerJson = ComposerJson::fromFile($composerJsonFile);

        // Read composer.lock
        $composerLock = ComposerLock::fromFile($composerLockFile);

        // Set project name
        if ($projectName === null) {
            $projectName = $composerJson->getName();
        }

        $project = new Project(
            $projectName,
            $projectSource,
            $projectState
        );
        $project->setComposerJson($composerJson);
        $project->setComposerLock($composerLock);

        // Rename project dir
        $projectDownloadDir = $this->getProjectDownloadDir($project);

        if (is_dir($projectDownloadDir)) {
            $command = 'rm -rf ' . $projectDownloadDir;
            system($command);
        }

        rename($tmpDownloadDir, $projectDownloadDir);

        return $project;
    }

    public function getProjectDownloadDir(Project $project = null): string
    {
        if ($project === null) {
            $projectName = Uuid::uuid4()->toString();
        } else {
            $projectName = preg_replace('/[^a-zA-Z0-9_-]/', '_', $project->getName());
        }
        return $this->gitTmpPath . DIRECTORY_SEPARATOR . $projectName;
    }

    public function updateProject(Project $project): Project
    {
        $updatedProject = $this->downloadProject(
            $project->getProjectState(),
            $project->getSource(),
            $project->getName()
        );

        $project->setComposerJson($updatedProject->getComposerJson());
        $project->setComposerLock($updatedProject->getComposerLock());

        return $project;
    }

    public function clearDownloadCache(): void
    {
        $files = scandir($this->gitTmpPath);

        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $command = 'rm -rf ' . $this->gitTmpPath . '/' . $file;
            system($command);
        }
    }
}