<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\PackagistData;
use App\Entity\Project;
use DateTime;

/**
 * Class ProjectResolveService
 *
 * @author Joachim Beig
 */
class ProjectResolveService
{
    public function addPackagistDataToProject(Project $project): void
    {
        $packagistData = $this->resolvePackagist($project->getName());

        if ($packagistData !== null) {
            $project->setLatestVersion($packagistData->getLatestVersion());
            $project->setLatestVersionTime($packagistData->getLatestVersionTime());
            $project->setAbandoned($packagistData->isAbandoned());
            $project->setAbandonedRecommendation($packagistData->getAbandonedRecommendation());
        }
    }

    public function resolvePackagist(string $packageName): ?PackagistData
    {
        $packagistUrl = sprintf('https://repo.packagist.org/p2/%s.json', $packageName);

        $curl = curl_init($packagistUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] !== 200) {
            return null;
        }

        $packages = json_decode($response, true);

        if (isset($packages['packages']) === false || isset($packages['packages'][$packageName]) === false) {
            return null;
        }

        $latestVersion = $packages['packages'][$packageName][0];
        $abandoned = isset($latestVersion['abandoned']);
        $abandonedVersion = null;
        if ($abandoned === true && is_string($latestVersion['abandoned'])) {
            $abandonedVersion = $latestVersion['abandoned'];
        }

        return new PackagistData(
            $packageName,
            $latestVersion['version'],
            new DateTime($latestVersion['time']),
            $abandoned,
            $abandonedVersion
        );
    }
}