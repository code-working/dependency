<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Class Package
 *
 * @author Joachim Beig
 */
class Package
{
    private string $name;
    private string $version;
    private string $source;
    private bool $dev;

    public function __construct(string $name, string $version, string $source, bool $dev)
    {
        $this->name = $name;
        $this->version = $version;
        $this->source = $source;
        $this->dev = $dev;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function isDev(): bool
    {
        return $this->dev;
    }
}