<?php

declare(strict_types=1);

namespace App\Entity;

class RequiredProject
{
    private Project $project;

    private string $installedVersion;

    private array $requiredBy;

    public function __construct(Project $project, string $installedVersion)
    {
        $this->project = $project;
        $this->installedVersion = $installedVersion;
    }

    private function addRequiredBy(ProjectDependency $projectDependency): void {



    }

}