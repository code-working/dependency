<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeInterface;

class PackagistData
{
    private string $packageName;
    private string $latestVersion;
    private DateTimeInterface $latestVersionTime;
    private bool $abandoned;
    private ?string $abandonedRecommendation;

    public function __construct(
        string $packageName,
        string $latestVersion,
        DateTimeInterface $latestVersionTime,
        bool $abandoned,
        ?string $abandonedRecommendation
    ) {
        $this->packageName = $packageName;
        $this->latestVersion = $latestVersion;
        $this->latestVersionTime = $latestVersionTime;
        $this->abandoned = $abandoned;
        $this->abandonedRecommendation = $abandonedRecommendation;
    }

    public function getPackageName(): string
    {
        return $this->packageName;
    }

    public function setPackageName(string $packageName): void
    {
        $this->packageName = $packageName;
    }

    public function getLatestVersion(): string
    {
        return $this->latestVersion;
    }

    public function setLatestVersion(string $latestVersion): void
    {
        $this->latestVersion = $latestVersion;
    }

    public function getLatestVersionTime(): DateTimeInterface
    {
        return $this->latestVersionTime;
    }

    public function setLatestVersionTime(DateTimeInterface $latestVersionTime): void
    {
        $this->latestVersionTime = $latestVersionTime;
    }

    public function isAbandoned(): bool
    {
        return $this->abandoned;
    }

    public function setAbandoned(bool $abandoned): void
    {
        $this->abandoned = $abandoned;
    }

    public function getAbandonedRecommendation(): ?string
    {
        return $this->abandonedRecommendation;
    }

    public function setAbandonedRecommendation(?string $abandonedRecommendation): void
    {
        $this->abandonedRecommendation = $abandonedRecommendation;
    }

}