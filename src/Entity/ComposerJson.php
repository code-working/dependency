<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Enum\ProjectType;

/**
 * Class ComposerJson
 *
 * @author Joachim Beig
 */
class ComposerJson
{
    public const FILE_NAME = 'composer.json';

    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getName(): ?string
    {
        return $this->data['name'] ?? null;
    }

    public function getType(): ?ProjectType
    {
        if (!isset($this->data['type'])) {
            return null;
        }
        if (!ProjectType::isValid($this->data['type'])) {
            return null;
        }
        return new ProjectType($this->data['type']);
    }

    public function getRequire(): array
    {
        return $this->data['require'] ?? [];
    }

    public function getRequireDev(): array
    {
        return $this->data['require-dev'] ?? [];
    }

    public function getAllRequired(): array
    {
        return array_merge($this->getRequire(), $this->getRequireDev());
    }

    public static function fromFile(string $file): ComposerJson
    {
        if (is_file($file) === true) {
            return new ComposerJson(json_decode(file_get_contents($file), true));
        }

        return new ComposerJson([]);
    }
}