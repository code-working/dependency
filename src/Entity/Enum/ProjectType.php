<?php

declare(strict_types=1);

namespace App\Entity\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class ProjectType
 *
 * @author Joachim Beig
 *
 * @method static ProjectType PROJECT()
 * @method static ProjectType LIBRARY()
 * @method static ProjectType METAPACKAGE()
 * @method static ProjectType COMPOSER_PLUGIN()
 *
 */
class ProjectType extends Enum
{
    public const PROJECT = 'project';
    public const LIBRARY = 'library';
    public const METAPACKAGE = 'metapackage';
    public const COMPOSER_PLUGIN = 'composer-plugin';
}