<?php

declare(strict_types=1);

namespace App\Entity\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class ProjectState
 *
 * @author Joachim Beig
 *
 * @method static ProjectState DIRECT()
 * @method static ProjectState DEPENDENCY()
 */
class ProjectState extends Enum
{
    public const DIRECT = 'direct';
    public const DEPENDENCY = 'dependency';
}