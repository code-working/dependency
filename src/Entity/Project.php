<?php

namespace App\Entity;

use App\Entity\Enum\ProjectState;
use App\Repository\ProjectRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(indexes={@ORM\Index(columns={"name"})})
 */
class Project
{
    /**
     * @var string|null
     * @ORM\Id()
     * @ORM\Column(type="string",  length=36, unique=true)
     */
    private ?string $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private string $source;

    /**
     * @ORM\Column(type=ProjectState::class, length=50)
     */
    private ProjectState $projectState;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private string $latestVersion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private DateTimeInterface $latestVersionTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private bool $abandoned;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $abandonedRecommendation;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $modified = null;

    private ?ComposerJson $composerJson;

    private ?ComposerLock $composerLock;

    public function __construct(
        string $name,
        string $source,
        ProjectState $projectState
    ) {
        $this->name = $name;
        $this->source = $source;
        $this->projectState = $projectState;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function getProjectState(): ?ProjectState
    {
        return $this->projectState;
    }

    public function setProjectState(ProjectState $projectState): void
    {
        $this->projectState = $projectState;
    }

    public function getLatestVersion(): string
    {
        return $this->latestVersion;
    }

    public function setLatestVersion(string $latestVersion): void
    {
        $this->latestVersion = $latestVersion;
    }

    public function getLatestVersionTime(): DateTimeInterface
    {
        return $this->latestVersionTime;
    }

    public function setLatestVersionTime(DateTimeInterface $latestVersionTime): void
    {
        $this->latestVersionTime = $latestVersionTime;
    }

    public function isAbandoned(): bool
    {
        return $this->abandoned;
    }

    public function setAbandoned(bool $abandoned): void
    {
        $this->abandoned = $abandoned;
    }

    public function getAbandonedRecommendation(): ?string
    {
        return $this->abandonedRecommendation;
    }

    public function setAbandonedRecommendation(?string $abandonedRecommendation): void
    {
        $this->abandonedRecommendation = $abandonedRecommendation;
    }

    public function getModified(): ?DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(DateTimeInterface $modified): void
    {
        $this->modified = $modified;
    }

    public function getComposerJson(): ?ComposerJson
    {
        return $this->composerJson;
    }

    public function setComposerJson(?ComposerJson $composerJson): void
    {
        $this->composerJson = $composerJson;
    }

    public function getComposerLock(): ?ComposerLock
    {
        return $this->composerLock;
    }

    public function setComposerLock(?ComposerLock $composerLock): void
    {
        $this->composerLock = $composerLock;
    }

    /**
     * @ORM\PrePersist()
     */
    public function createId(): void
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModified(): void
    {
        $this->modified = new DateTime();
    }
}
