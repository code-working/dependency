<?php

namespace App\Entity;

use App\Repository\ProjectDependencyRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass=ProjectDependencyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"project_scope"}),
 *     @ORM\Index(columns={"project"}),
 *     @ORM\Index(columns={"requires"})
 * })
 */
class ProjectDependency
{
    /**
     * @var string|null
     * @ORM\Id()
     * @ORM\Column(type="string",  length=36, unique=true)
     */
    private ?string $id;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private string $projectScope;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private string $project;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private string $requires;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $requireDev;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $requiredVersion;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $installedVersion;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $modified = null;

    public function __construct(
        string $projectScope,
        string $project,
        string $requires,
        bool $requireDev,
        string $requiredVersion,
        string $installedVersion
    ) {
        $this->projectScope = $projectScope;
        $this->project = $project;
        $this->requires = $requires;
        $this->requireDev = $requireDev;
        $this->requiredVersion = $requiredVersion;
        $this->installedVersion = $installedVersion;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getProjectScope(): string
    {
        return $this->projectScope;
    }

    public function setProjectScope(string $projectScope): void
    {
        $this->projectScope = $projectScope;
    }

    public function getProject(): string
    {
        return $this->project;
    }

    public function setProject(string $project): void
    {
        $this->project = $project;
    }

    public function getRequires(): string
    {
        return $this->requires;
    }

    public function setRequires(string $requires): void
    {
        $this->requires = $requires;
    }

    public function getRequireDev(): bool
    {
        return $this->requireDev;
    }

    public function setRequireDev(bool $requireDev): void
    {
        $this->requireDev = $requireDev;
    }

    public function getRequiredVersion(): string
    {
        return $this->requiredVersion;
    }

    public function setRequiredVersion(string $requiredVersion): void
    {
        $this->requiredVersion = $requiredVersion;
    }

    public function getInstalledVersion(): string
    {
        return $this->installedVersion;
    }

    public function setInstalledVersion(string $installedVersion): void
    {
        $this->installedVersion = $installedVersion;
    }

    public function getModified(): ?DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(DateTimeInterface $modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @ORM\PrePersist()
     */
    public function createId(): void
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModified(): void
    {
        $this->modified = new DateTime();
    }
}
