<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Class ComposerLock
 *
 * @author Joachim Beig
 */
class ComposerLock
{
    public const FILE_NAME = 'composer.lock';

    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /** @return Package[] */
    public function getPackages(): array
    {
        if (isset($this->data['packages']) === false) {
            return [];
        }

        $packages = [];

        foreach ($this->data['packages'] as $package) {
            $packages[$package['name']] = new Package(
                $package['name'],
                $package['version'],
                $package['source']['url'],
                false
            );
        }

        return $packages;
    }

    /** @return Package[] */
    public function getPackagesDev(): array
    {
        if (isset($this->data['packages-dev']) === false) {
            return [];
        }

        $packages = [];

        foreach ($this->data['packages-dev'] as $package) {
            $packages[$package['name']] = new Package(
                $package['name'],
                $package['version'],
                $package['source']['url'],
                true
            );
        }

        return $packages;
    }

    /** @return Package[] */
    public function getAllPackages(): array
    {
        return array_merge($this->getPackages(), $this->getPackagesDev());
    }

    public static function fromFile(string $file): ComposerLock
    {
        if (is_file($file) === true) {
            return new ComposerLock(json_decode(file_get_contents($file), true));
        }

        return new ComposerLock([]);
    }
}