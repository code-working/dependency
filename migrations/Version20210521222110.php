<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210521222110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE project (id VARCHAR(36) NOT NULL, name VARCHAR(255) NOT NULL, source VARCHAR(1024) NOT NULL, project_state VARCHAR(50) NOT NULL --(DC2Type:App\\Entity\\Enum\\ProjectState)
        , latest_version VARCHAR(50) DEFAULT NULL, latest_version_time DATETIME DEFAULT NULL, abandoned BOOLEAN DEFAULT NULL, abandoned_recommendation VARCHAR(255) DEFAULT NULL, modified DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE5E237E06 ON project (name)');
        $this->addSql('CREATE TABLE project_dependency (id VARCHAR(36) NOT NULL, project_scope VARCHAR(36) NOT NULL, project VARCHAR(36) NOT NULL, requires VARCHAR(36) NOT NULL, require_dev BOOLEAN NOT NULL, required_version VARCHAR(50) NOT NULL, installed_version VARCHAR(50) NOT NULL, modified DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_37FE2E3713FA5C4D ON project_dependency (project_scope)');
        $this->addSql('CREATE INDEX IDX_37FE2E372FB3D0EE ON project_dependency (project)');
        $this->addSql('CREATE INDEX IDX_37FE2E37634820CE ON project_dependency (requires)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_dependency');
    }
}
